# Ionburst Cloud Changelog

## September 2022

### Ionburst Cloud Platform

#### ✨ New Features

-	Account e-mail is now displayed on confirmation screen.
-	Feedback and account deletion sections have been added to the account details page.

#### ⚙️ Fixes

-	Styling and bug fixes for sign up and sign in.

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- Engineering blog: [Large objects are now supported on IBC S6](https://ionburst.cloud/blog/introducing-large-object-support-for-ibc-s6)
- Developer tutorial: [Getting started with Send and IBC S6](https://docs.ionburst.cloud/tutorials/get-started-with-ibc-s6-firefox-send)
- Developer tutorial: [IonFS CLI: IBC S6 quickstart guide](https://docs.ionburst.cloud/tutorials/ionfs-cli-ibc-s6-quickstart)
- Developer tutorial: [IonFS CLI: IBC NKV quickstart guide](https://docs.ionburst.cloud/tutorials/ionfs-cli-ibc-nkv-quickstart)
- Developer tutorial: [Using the new HEAD API](https://docs.ionburst.cloud/tutorials/get-started-with-ionburst-cloud-api-head-method)
- Developer tutorial: [Getting started with IBC S6 manifests](https://docs.ionburst.cloud/tutorials/get-started-with-ibc-s6-sdk-manifests)

#### ⚙️ Fixes

- Site-wide updates and fixes on our website and docs.

### Ionburst Cloud Clients & Integrations

#### 🚀 New Releases

- Version 1.3.0 of our [.NET SDK](https://github.com/ionburstcloud/ionburst-sdk-net) is now available, adding support for the new HEAD method, and large object uploads with SDK manifests.
- Version 1.2.0 of our [Go SDK](https://github.com/ionburstcloud/ionburst-sdk-go) is now available, adding support for the new HEAD method, and large object uploads with SDK manifests.
- Version 1.2.0 of the [ioncli](https://github.com/ionburstcloud/ionburst-sdk-go/tree/main/ioncli) tool is now available, adding support for the new HEAD method, and large object uploads with SDK manifests.
- Version 0.3.0 of the [IonFS CLI](https://github.com/ionburstcloud/IonFS) is now available, adding support for local filesystem metadata repositories, and large object uploads with the SDK manifests feature.
- Our [updated fork of the Firefox Send](https://github.com/ionburstcloud/send) project is now live.
- We've merged our homebrew formulae into a [single tap](https://github.com/ionburstcloud/homebrew-tools), supporting IonFS CLI, ioncli and namegen.

## August 2022

### Ionburst Cloud Website & Docs

#### ✨ New Features

- Usage counters on the website now update regularly.

#### ⚙️ Fixes

- Site-wide updates and fixes on our website and docs.

### Ionburst Cloud Clients & Integrations

#### ✨ New Features

- Large object support using the new SDK manifests feature has been added to IonFS CLI.
- Initial work on large object support for the Go SDK has begun in 1.2.0-develop.6

#### ⚙️ Fixes

- Dependency and bug fixes for IonFS CLI.

## July 2022

### Ionburst Cloud Platform

#### 🚀 New Releases

- A new `HEAD` method has been added to the IBC S6 and IBC NKV services, allowing objects and secrets to be verified after upload.

#### ✨ New Features

- The Ionburst Cloud Platform no longer requires a private beta code on sign up.
- We've added some useful information on sign up, and first sign-in to improve the overall developer experience.

#### ⚙️ Fixes

- Styling and bug fixes for the Ionburst Cloud Platform.

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- Product pages for [IBC S6](https://ionburst.cloud/ibc-s6) and [IBC NKV](https://ionburst.cloud/ibc-nkv), and a [how it works](https://ionburst.cloud/how-it-works) page have been added to the website.

#### ✨ New Features

- API Docs for our new `HEAD` method are now available.

#### ⚙️ Fixes

- Site-wide updates and fixes on our website and docs.
  
### Ionburst Cloud Clients & Integrations

#### 🚀 New Releases

- Version 1.1.1 of our Node.js and Python SDKs are now available.
- A [homebrew formula for ioncli](https://github.com/ionburstcloud/homebrew-tools) is now available for the `develop` branch.

#### ✨ New Features

- Support for our new `HEAD` method is now available for:
  - The .NET SDK, from version 1.3.0-develop.3
  - The Node.js SDK, from version 1.2.0-develop.1
  - The Python SDK, from version 1.2.0-develop.1
  - The Go SDK, from version 1.2.0-develop.1
  - ioncli, from version 1.2.0-develop.1
- Initial work on large object support for our .NET SDK has begun in 1.3.0-develop.12

#### ⚙️ Fixes

- Dependency updates to the:
  - Node.js SDK
  - Python SDK

## June 2022

### Ionburst Cloud Platform

#### 🚀 New Releases

- The Ionburst Cloud Platform no longer requires customer address or payment details on sign-up, and instead provides a restricted free tier.
- The new Ionburst Cloud Platform UI using TailwindCSS has been deployed.

#### ✨ New Features

- Service usage for each instance is now displayed on the dashboard.

#### ⚙️ Fixes

- Styling and bug fixes to support the new portal skin.

## May 2022

### Ionburst Cloud Website & Docs

#### ⚙️ Fixes

- Node.js SDK docs were showing the wrong function for `checkDeferredAsync`
- Minor content and grammar fixes on our API docs.

### Ionburst Cloud Clients & Integrations

#### ✨ New Features

- Work to add support for the new HEAD API method in the .NET SDK has been added to 1.3.0-develop.3

#### ⚙️ Fixes

- Node.js SDK README updates to use correct function for `checkDeferredAsync` example.

## April 2022

### Ionburst Cloud Platform

#### 🚀 New Releases

- The Ionburst Cloud Platform Private Beta has been released, with support for billing through Stripe and detailed service usage.

#### ✨ New Features

- We've added new pages to the portal for service usage and account details.
- A description field can now be added when creating a party.
- The party management page now displays the date created for each party.
- API credentials creation now has full clipboard support.

#### ⚙️ Fixes

- The usage page now displays the party name and region.
- Styling and bug fixes to support the Private Beta release.

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- Our new [status website](https://status.ionburst.cloud) has been released.

### Ionburst Cloud Clients & Integrations

#### 🚀 New Releases

- ioncli 1.1.2 is now available.
- The first release of our [namegen](https://github.com/ionburstcloud/namegen) tool is now available, with [homebrew install available](https://github.com/ionburstcloud/homebrew-tools) for Mac users.

#### ⚙️ Fixes

- The Go SDK has received dependency updates in 1.1.2

## March 2022

### Ionburst Cloud Clients & Integrations

#### ✨ New Features

- The local filesystem metadata repository is now available in the `develop` branch of IonFS CLI.
- Mac builds in the `develop` branch of the IonFS CLI are now notarized.
- `0.3.0-develop` versions of IonFS CLI are now available from the homebrew cask.
- Work to improve .NET SDK client creation and flexibility handling API credentials has been added to 1.3.0-develop.1

#### ⚙️ Fixes

- The .NET SDK has received dependency updates in 1.2.6
- The Node.js SDK has been updated to improve its ability to get the project root directory.

## February 2022

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- The new [Ionburst Cloud website](https://ionburst.cloud) has been released.
- Our documentation site is now available at [docs.ionburst.cloud](https://docs.ionburst.cloud)

#### ✨ New Features

- The IonFS CLI install instructions have been updated to [include homebrew](https://docs.ionburst.cloud/ionfs/install#install-with-homebrew) as an option for Mac users.

#### ⚙️ Fixes

- Site-wide updates and fixes on our new website, and docs.

## December 2021

### Ionburst Cloud Clients & Integrations

#### ✨ New Features

- The initial implementation for a local filesystem metadata repository has been added to the IonFS CLI.

#### ⚙️ Fixes

- The IonFS CLI has received further bug fixes and dependency updates to support .NET 6.0

## November 2021

### Ionburst Cloud Clients & Integrations

#### ⚙️ Fixes

- Initial work has been done to upgrade the IonFS CLI to .NET 6.0

## August 2021

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- Ionburst Cloud is now an AWS Advanced Technology Partner, with our Dublin (eu-west-1) and London (eu-west-2) regions successfully [passing the AWS Foundational Technical Review](https://ionburst.cloud/highlights/ionburst-cloud-successfully-passes-aws-ftr).

### Ionburst Cloud Clients & Integrations

#### ⚙️ Fixes

- The .NET SDK has received dependency updates and bug fixes in 1.2.5

## July 2021

### Ionburst Cloud Platform

#### 🚀 New Releases

- Our IBC No Key Vault (NKV) Service is now available in our eu-west-1 and eu-west-2 regions.

#### ✨ New Features

- We've added a Dashboard area into the Portal.
- We've added a Parties page, so developers can view available parties.

#### ⚙️ Fixes

- The Ionburst Cloud Portal is now available on [ionburst.cloud](https://portal.ionburst.cloud).
- The left sidebar in the Portal has been updated to include icons, and the menu has a collapsable toggle.
- Resource links have been added to the lower left sidebar to provide easy access to important resources.
- Personalised greeting added to dashboard area.
- The topbar in the Portal has been updated to include an account details dropdown and sign out functionality.

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- IBC NKV [common use scenarios](https://ionburst.cloud/docs/common-use-scenarios#no-key-vault-nkv) added.
- Catch up with the [IBC NKV: Securely store, access, and distribute secrets](https://ionburst.cloud/highlights/nkv-ultra-secure-secrets-storage-service) announcement.
- Our developer tutorial, [getting started with the IonFS CLI, S6 and AWS S3](https://ionburst.cloud/tutorials/get-started-with-ioncli), is now live.
- Our developer tutorial, [getting started with the IonFS CLI, NKV and AWS S3](https://ionburst.cloud/tutorials/get-started-with-ioncli), is now live.
- We've updated our [API documentation](https://ionburst.cloud/docs/secrets) with details on NKV.
- We've added a configuration section to the IonFS CLI docs for setting up with [IBC S6](https://ionburst.cloud/docs/ionfs/s6-configuration) and [NKV](https://ionburst.cloud/docs/ionfs/nkv-configuration).
- We've added a configuration section to the ioncli docs for setting up with [IBC S6](https://ionburst.cloud/docs/ioncli/s6-configuration).

#### ⚙️ Fixes

- We've updated some minor styling issues on the blog page.
- Fixed command outputs and the structure of our `ioncli` tutorial.
- The IonFS CLI and ioncli install guides now link to the latest release available.
- Site-wide grammar and terminology updates for IBC S6 and IBC NKV.

### Ionburst Cloud Clients & Integrations

#### ✨ New Features

- IonFS now has a homebrew cask, and can be [installed with brew](https://github.com/ionburstcloud/homebrew-ionfs).
- IBC NKV support is now available for:
  - [IonFS](https://github.com/ionburstcloud/IonFS), from version 0.2.0
  - [ioncli](https://github.com/ionburstcloud/ionburst-sdk-go), from version 1.1.0
  - The [Ionburst .NET SDK](https://github.com/ionburstcloud/ionburst-sdk-net), from version 1.2.0
  - The [Ionburst Node.js SDK](https://github.com/ionburstcloud/ionburst-sdk-javascript), from version 1.1.0
  - The [Ionburst Python SDK](https://github.com/ionburstcloud/ionburst-sdk-python), from version 1.1.0
  - The [Ionburst Go SDK](https://github.com/ionburstcloud/ionburst-sdk-go), from version 1.1.0

#### ⚙️ Fixes

- Minor fixes and updates to SDK and client README files.
- The latest releases for IonFS CLI and ioncli now include a download alias link to the latest version.
- All SDKs have had minor dependency updates.

## June 2021

### Ionburst Cloud Website & Docs

#### 🚀 New Releases

- v1 of the [Ionburst Cloud Shared Responsibility Model](https://ionburst.cloud/docs/shared-responsibility-model) has now been published.
- The [Introducing Ionburst Cloud](https://ionburst.cloud/blog/introducing-ionburst-cloud) blog is now available.
- Our first developer tutorial, [getting started with ioncli](https://ionburst.cloud/tutorials/get-started-with-ioncli), is now live.

#### ✨ New Features

- We've added a [highlights section](https://ionburst.cloud/highlights) to our site to keep track of exciting platform news and updates.
- We've added a [tutorials section](https://ionburst.cloud/tutorials) to our site where you can learn how to get started with Ionburst Cloud tools and integrations.
- Ionburst Cloud now has a [GitHub organisation](https://github.com/ionburstcloud).

#### ⚙️ Fixes

- We've updated our [SDK documentation](https://ionburst.cloud/docs/sdk) with some minor grammatical and structural changes.

### Ionburst Cloud Clients & Integrations

#### ✨ New Features

- The Ionburst Cloud SDK repositories are now mirrored to our new [GitHub](https://github.com/ionburstcloud).
